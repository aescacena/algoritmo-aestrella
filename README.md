# Actividad :  Resolución de problema mediante búsqueda heurística

## Objetivos de la actividad

Con esta actividad vas a conseguir implementar la estrategia de búsqueda heurística A* para la resolución de un problema real.

## Descripción de la actividad

![texto alternativo](https://drive.google.com/uc?export=view&id=1nku190wKb1Wn8V-d_X8RVoREuKjavF0I)

La empresa Amazon desea utilizar un robot para ordenar el inventario de su almacén. 

Amazon cuenta con 3 inventarios (mesa con suministros para vender) localizados en unas posiciones específicas del almacén. El robot se debe encargar de mover los 3 inventarios a una posición objetivo. 

El robot puede moverse horizontal y verticalmente, y cargar o descargar un inventario. 

Un ejemplo del robot, moviendo el inventario, se puede observar en el siguiente vídeo:

## Estado inicial

El estado inicial del problema lo vamos a representar en una mátriz 4x4 de carácteres de la siguiente manera:


![texto alternativo](imagenes/EstadoInicial.JPG)		

Donde, 


*   R: representa el robot. Inicialmente está ubicado en la posición [2,2]
*   \#: representa una pared. 
*   M1, M2, e M3: representan los tres inventarios que el robot debe mover. Y se encuentran ubicadas en las posiciones [0,0], [2,0] y [0,3] respectivamente.


## Estado Objetivo 

El robot debe mover los 3 inventarios, M1, M2 y M3, a la siguientes posiciones:


![texto alternativo](imagenes/EstadoObjetivo.JPG)

## Tareas a realizar 

Implementar el algoritmo A* considerando lo siguiente:

1.   como función heurística, la Distancia en Manhattan. 
2.   el coste real (g) de cada acción del robot es 1.
3.   el código deberá ejecutarse e indicar la secuencia de acciones a realizar para alcanzar el estado objetivo utilizando una notación sencilla. Por ejemplo: «mover R fila1 columna2» o «mover R fila0 columna2» o «cargar R M1 fila0 columna2».