# -*- coding: utf-8 -*-
"""
Created on Sun Apr 25 19:23:23 2021

@author: STE
"""

class Robot:
    
    def __init__(self, nombre):
        """ Argumentos:
                nombre -- nombre de robot
        """
        self.nombre = nombre
        self.ocupado = False
        self.inventario = None
    
    def getNombre(self):
        """ Devuelve Nombre """
        return self.nombre
    
    def setNombre(self, nombre):
        """ Asigna Nombre """
        self.nombre = nombre
    
    def getEstado(self):
        """Devuelve estado """
        return self.ocupado
    
    def setEstado(self, ocupado):
        """Devuelve estado """
        self.ocupado = ocupado
        
    def getInventario(self):
        """Devuelve Inventario"""
        return self.inventario
    
    def setInventario(self, inventario):
        """Asigna inventario a Robot"""
        self.inventario = inventario
        self.ocupado = True
    