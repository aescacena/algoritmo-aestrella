# -*- coding: utf-8 -*-
"""
Created on Tue Apr 27 10:55:31 2021

@author: STE
"""


class Nodo:

    def __init__(self, x, y):
        """ Argumentos:
                x -- posicion en X
                y -- posicion en Y
        """
        self.x = x
        self.y = y
        self.g = 0
        self.h = 0
        self.f = 0
        self.ocupado = False
        self.tipo = None  # 0:Pared, 1:Inventario, 2:Robot, 3:Robot cargado, 4: Robot en misma posición que inventario pero sin estar cargado
        self.nodoSucesor = None

    # Devuelve posicion en X
    def getX(self):
        return self.x

    # Devuelve posicion en Y
    def getY(self):
        return self.y

    # Devuelve posicion en X e Y
    def getPosicion(self):
        return (self.x, self.y)

    # Devuelve valor de Evaluación
    def getEvaluacion(self):
        return self.g + self.h

    # Devuelve coste operativo
    def getCosteOperativo(self):
        return self.g

    # Asigna nuevo valor operativo
    def setCosteOperativo(self, g):
        self.g = g

    # Devuelve la Heuristica
    def getHeuristica(self):
        return self.h

    # Asigna nuevos valores operativo y heuristica
    def setCostes(self, g, h):
        self.g = g
        self.h = h
        self.f = g + h

    # Se indica Nodo ocupado y tipo de ocupación
    def ocuparNodo(self, tipo):
        self.ocupado = True
        self.tipo = tipo

    # Se indica que Nodo ya no está ocupado
    def desocuparNodo(self):
        self.ocupado = False
        self.tipo = None

    # Devuelve tipo de ocupación
    def getTipoOcupado(self):
        return self.tipo

    # Devuelve si Nodo está ocupado
    def nodoOcupado(self):
        return self.ocupado

    # Se asigna Nodo sucesor al actual
    def setNodoSucesor(self, nodo):
        self.nodoSucesor = nodo

    # Devuelve Nodo sucesor al actual
    def getNodoSucesor(self):
        return self.nodoSucesor