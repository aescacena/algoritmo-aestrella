
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 25 19:23:23 2021

@author: STE
"""

class Inventario:
    
    def __init__(self, nombre):
        """ Argumentos:
                nombre -- nombre de robot
        """
        self.nombre = nombre
    
    def getNombre(self):
        """Devuelve nombre de inventario"""
        return self.nombre
    
    def setNombre(self, nombre):
        """Asigna nuevo nombre al inventario"""
        self.nombre = nombre