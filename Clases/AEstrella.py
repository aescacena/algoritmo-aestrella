
"""
[x][y]
[x] -> Vertical en la matriz
[y] -> Horizontal en la matriz
"""

class AEstrella:

    def __init__(self, mapa, nodoOrigen, nodoObjetivo):
        self.mapa = mapa

        self.nodoOrigen = nodoOrigen
        self.nodoOrigen.setCostes(0,0)  #Nos aseguramos que tiene costes "0"
        self.nodoOrigen.setNodoSucesor(None)    #Nos aseguramos que tiene Nodo sucesor asociado

        self.nodoObjetivo = nodoObjetivo
        self.nodoObjetivo.setCostes(0, 0)   #Nos aseguramos que tiene costes "0"
        self.nodoObjetivo.setNodoSucesor(None)  #Nos aseguramos que tiene Nodo sucesor asociado

        self.listaAbierta = [self.nodoOrigen]   #Asignamos Nodo origen como primer valor de la Lista Abierta
        self.listaCerrada = []
        self.rutaEncontrada = self.buscarRuta()

    # Devuelve Nodo Origen
    def getNodoOrigen(self):
        return self.nodoOrigen

    # Devuelve Nodo Objetivo
    def getNodoObjetivo(self):
        return self.nodoObjetivo

    # Devuelve el siguiente Nodo con menor Evaluación
    def getNodo(self):
        nodo = -1
        # Si Lista abierta está vacía devolvemos -1
        if len(self.listaAbierta) <= 0:
            return -1
        # En caso contrario
        else:
            nodo = self.listaAbierta[0]

        # Buscamos el Nodo con menor valor de Evaluación
        for nodoAux in self.listaAbierta:
            if nodoAux.getEvaluacion() < nodo.getEvaluacion():
                nodo = nodoAux

        return nodo

    # Comprueba si el Nodo recibido por parámetros es un posible nuevo Sucesor
    def esNodoSucesor(self, nodo):
        res = False
        # Se comprueba si aún no ha sido Nodo de paso o candidato
        if not (nodo in self.listaAbierta) and not (nodo in self.listaCerrada) and (nodo.getTipoOcupado() != 0):
            # Si Origen es Robot no cargado
            if self.nodoOrigen.nodoOcupado() and (self.nodoOrigen.getTipoOcupado() == 2 or self.nodoOrigen.getTipoOcupado() == 4):
                # Si Nodo no ocupado
                if not nodo.nodoOcupado():
                    res = True
                # Si Nodo ocupado con almacén (podemos pasar bajo el almacén)
                elif (nodo.nodoOcupado()) and (nodo.getTipoOcupado() == 1 or nodo.getTipoOcupado() == None):
                    res = True
            # Si Origen es Robot cargado con Inventario
            elif self.nodoOrigen.nodoOcupado() and self.nodoOrigen.getTipoOcupado() == 3:
                # Si Nodo no ocupado
                if not nodo.nodoOcupado():
                    res = True
        return res

    # Acualiza costes del Nodo sucesor recibido por parámetros
    def actualizaSucesor(self, nodoSucesor, nodoActual):
        # Comprueba si Nodo ya existe en Lista Abierta
        if nodoSucesor in self.listaAbierta:
            # Si el nuevo Coste operativo es menor que su valor actual lo actualizamos
            if nodoActual.getCosteOperativo() + 1 < nodoSucesor.getCosteOperativo():
                nodoSucesor.setCostes(nodoActual.getCosteOperativo() + 1, self.getHeuristica(nodoSucesor))
                nodoSucesor.setNodoSucesor(nodoActual)  # Indicamos Nodo del que viene
        # Si no, asigna Coste, Heuristica y lo asignamos como Nodo Sucesor
        else:
            nodoSucesor.setCostes(nodoActual.getCosteOperativo() + 1, self.getHeuristica(nodoSucesor))
            nodoSucesor.setNodoSucesor(nodoActual)  # Indicamos Nodo del que viene

    # Obtenemos la heuristica real desde nodo actual hasta nodo objetivo
    def getHeuristica(self, nodoActual):
        # Si la posición es distinta
        if nodoActual.getX() != self.nodoObjetivo.getX() or nodoActual.getY() != self.nodoObjetivo.getY():
            x = abs(nodoActual.getX() - self.nodoObjetivo.getX())
            y = abs(nodoActual.getY() - self.nodoObjetivo.getY())
            return x + y
        else:
            return 0

    # Imprime posición (X, Y) de Nodos en la lista
    def imprimeLista(self, lista):
        for i in lista:
            print(i.getPosicion())

    # Obtenemos lista de Nodos de la ruta calculada
    def getListRuta(self):
        rutaCompleta = False
        listaRuta = []
        siguienteNodo = self.nodoObjetivo

        while not rutaCompleta:
            listaRuta.append(siguienteNodo)
            siguienteNodo = siguienteNodo.getNodoSucesor()

            if siguienteNodo == None:
                rutaCompleta = True

        return listaRuta

    # Devuelve lista abierta en el estado actual
    def getListaAbierta(self):
        return self.listaAbierta

    # Devuelve lista cerrada en el estado actual
    def getListaCerrada(self):
        return self.listaCerrada

    # Devuelve si la Ruta fue encontrada
    def isRutaEncontrada(self):
        return self.rutaEncontrada

    # Realiza la busqueda desde Nodo Origen al Nodo Destino
    def buscarRuta(self):
        objetivo = 0 # 1: Objetivo encontrado, -1: Objetivo no encontrado
        nodoActual = None
        nodo = None

        while objetivo == 0:
            # Comprobamos que aún hay Nodos disponibles
            if len(self.listaAbierta) == 0:
                objetivo = -1
                return False
            # Obtenemos siguiente Nodo
            else:
                nodo = self.getNodo()
                # Si nos devuelve un Nodo valido añadimos Nodo a Lista Cerrada y eliminamos de Lista Abierta
                if nodo != -1:
                    nodoActual = nodo
                    self.listaCerrada.append(nodo)
                    self.listaAbierta.remove(nodo)

            # Comprobamos si es Nodo Objetivo
            if nodoActual.getX() == self.nodoObjetivo.getX() and nodoActual.getY() == self.nodoObjetivo.getY():
                objetivo = 1
                return True

            # Si no, Generamos sucesores
            else:
                nodo = None
                if ((nodoActual.getX() + 1) <= 3):
                    nodo = self.mapa[nodoActual.getX() + 1][nodoActual.getY()]
                    if self.esNodoSucesor(nodo):
                        self.actualizaSucesor(nodo, nodoActual)
                        self.listaAbierta.append(nodo)
                        # print("Inserta en Lista Abierta: ", nodo.getPosicion())

                if ((nodoActual.getX() - 1) > -1):
                    nodo = self.mapa[nodoActual.getX() - 1][nodoActual.getY()]
                    if self.esNodoSucesor(nodo):
                        self.actualizaSucesor(nodo, nodoActual)
                        self.listaAbierta.append(nodo)
                        # print("Inserta en Lista Abierta: ", nodo.getPosicion())

                if ((nodoActual.getY() + 1) <= 3):
                    nodo = self.mapa[nodoActual.getX()][nodoActual.getY() + 1]
                    if self.esNodoSucesor(nodo):
                        self.actualizaSucesor(nodo, nodoActual)
                        self.listaAbierta.append(nodo)
                        # print("Inserta en Lista Abierta: ", nodo.getPosicion())

                if ((nodoActual.getY() - 1) > -1):
                    nodo = self.mapa[nodoActual.getX()][nodoActual.getY() - 1]
                    if self.esNodoSucesor(nodo):
                        self.actualizaSucesor(nodo, nodoActual)
                        self.listaAbierta.append(nodo)
                        # print("Inserta en Lista Abierta: ", nodo.getPosicion())
        if objetivo == 1:
            return True
        else:
            return False

    # Se imprime Ruta encontrada
    def imprimeRuta(self):
        if self.isRutaEncontrada():
            listaRuta = self.getListRuta()
            print("Ruta:")
            print("\t- Coste: ", listaRuta[0].getEvaluacion())
            listaRuta.reverse()
            for nodoAux in listaRuta:
                nodoSucesor = nodoAux.getNodoSucesor()
                if nodoSucesor != None:
                    print('\t\tMover R: ', nodoSucesor.getPosicion(), ' -> ', nodoAux.getPosicion())
        else:
            print("Ruta no encontrada")