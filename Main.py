# -*- coding: utf-8 -*-
"""
Created on Tue Apr 27 10:20:47 2021

@author: AEscacena
"""

from Clases.Nodo import Nodo
from Clases.AEstrella import AEstrella

# ----------------------------- BUSQUEDA INVENTARIO 1 -----------------------------------#

mapa1 = ([Nodo(0,0), Nodo(0,1), Nodo(0,2), Nodo(0,3)], [Nodo(1,0), Nodo(1,1), Nodo(1,2), Nodo(1,3)], [Nodo(2,0), Nodo(2,1), Nodo(2,2), Nodo(2,3)], [Nodo(3,0), Nodo(3,1), Nodo(3,2), Nodo(3,3)])
mapa1[0][1].ocuparNodo(0)
mapa1[1][1].ocuparNodo(0)
mapa1[0][0].ocuparNodo(1)
mapa1[2][0].ocuparNodo(1)
mapa1[0][3].ocuparNodo(1)

robot1 = mapa1[2][2]
robot1.ocuparNodo(2)
idaInventario1 = mapa1[0][0]
idaInventario1.ocuparNodo(1)

rutaIdaInventario_1 = AEstrella(mapa1, robot1, idaInventario1)
rutaIdaInventario_1.imprimeRuta()

robot1.desocuparNodo()
idaInventario1.ocuparNodo(3)
robot1 = idaInventario1

vueltaInventario1 = mapa1[3][3]

rutaVueltaInventario_1 = AEstrella(mapa1, robot1, vueltaInventario1)
rutaVueltaInventario_1.imprimeRuta()

# ----------------------------- BUSQUEDA INVENTARIO 2 -----------------------------------#

mapa2 = ([Nodo(0,0), Nodo(0,1), Nodo(0,2), Nodo(0,3)], [Nodo(1,0), Nodo(1,1), Nodo(1,2), Nodo(1,3)], [Nodo(2,0), Nodo(2,1), Nodo(2,2), Nodo(2,3)], [Nodo(3,0), Nodo(3,1), Nodo(3,2), Nodo(3,3)])
mapa2[0][1].ocuparNodo(0)
mapa2[1][1].ocuparNodo(0)
mapa2[0][0].ocuparNodo(1)
mapa2[2][0].ocuparNodo(1)
mapa2[0][3].ocuparNodo(1)

robot2 = mapa2[2][2]
robot2.ocuparNodo(2)
idaInventario2 = mapa2[2][0]
idaInventario2.ocuparNodo(1)

rutaIdaInventario_2 = AEstrella(mapa2, robot2, idaInventario2)
rutaIdaInventario_2.imprimeRuta()

robot2.desocuparNodo()
idaInventario2.ocuparNodo(3)
robot2 = idaInventario2

vueltaInventario2 = mapa2[3][2]
vueltaInventario2.desocuparNodo()

rutaVueltaInventario_2 = AEstrella(mapa2, robot2, vueltaInventario2)
rutaVueltaInventario_2.imprimeRuta()

# ----------------------------- BUSQUEDA INVENTARIO 3 -----------------------------------#

mapa3 = ([Nodo(0,0), Nodo(0,1), Nodo(0,2), Nodo(0,3)], [Nodo(1,0), Nodo(1,1), Nodo(1,2), Nodo(1,3)], [Nodo(2,0), Nodo(2,1), Nodo(2,2), Nodo(2,3)], [Nodo(3,0), Nodo(3,1), Nodo(3,2), Nodo(3,3)])
mapa3[0][1].ocuparNodo(0)
mapa3[1][1].ocuparNodo(0)
mapa3[0][0].ocuparNodo(1)
mapa3[2][0].ocuparNodo(1)
mapa3[0][3].ocuparNodo(1)

robot3 = mapa3[2][2]
robot3.ocuparNodo(2)
idaInventario3 = mapa3[0][3]
idaInventario3.ocuparNodo(1)

rutaIdaInventario_3 = AEstrella(mapa3, robot3, idaInventario3)
rutaIdaInventario_3.imprimeRuta()

robot3.desocuparNodo()
idaInventario3.ocuparNodo(3)
robot3 = idaInventario3

vueltaInventario3 = mapa3[3][1]
vueltaInventario3.desocuparNodo()

rutaVueltaInventario_3 = AEstrella(mapa3, robot3, vueltaInventario3)
rutaVueltaInventario_3.imprimeRuta()



# ****************************************** ITERACIÓN 2 ************************************ #

# -------------------------- Búsqueda Inventario 1 ------------------------ #

mapa = ([Nodo(0,0), Nodo(0,1), Nodo(0,2), Nodo(0,3)], [Nodo(1,0), Nodo(1,1), Nodo(1,2), Nodo(1,3)], [Nodo(2,0), Nodo(2,1), Nodo(2,2), Nodo(2,3)], [Nodo(3,0), Nodo(3,1), Nodo(3,2), Nodo(3,3)])
mapa[0][1].ocuparNodo(0) # Pared
mapa[1][1].ocuparNodo(0) # Pared
mapa[0][0].ocuparNodo(1) # M1
mapa[2][0].desocuparNodo() # Desocupar Nodo de inventario M2
mapa[0][3].ocuparNodo(1) # M3
mapa[2][2].desocuparNodo() #Desocupamos Nodo posición inicial Robot
mapa[3][2].ocuparNodo(4) #Ocupar Nodo con nueva posición de Robot

robot = mapa[3][2]
idaInventario = mapa[0][0]

rutaIdaInventario = AEstrella(mapa, robot, idaInventario)
print("IDA M1")
rutaIdaInventario.imprimeRuta()

robot.desocuparNodo()
robot.ocuparNodo(1)
idaInventario.ocuparNodo(3)
robot = idaInventario

vueltaInventario = mapa[3][3]
vueltaInventario.desocuparNodo()

rutaVueltaInventario = AEstrella(mapa, robot, vueltaInventario)
print("VUELTA M1")
rutaVueltaInventario.imprimeRuta()
